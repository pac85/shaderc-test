
extern crate shaderc;
use shaderc::*;

extern crate proc_macro;
use proc_macro::TokenStream;

#[proc_macro]
pub fn test(_input: TokenStream) -> TokenStream {
    let source = "#version 450\n #include <lol> \n void EP() {}";

    let mut compiler = Compiler::new().unwrap();
    let mut options = CompileOptions::new().unwrap();
    options.add_macro_definition("EP", Some("main"));
    options.set_include_callback(|name, type_, _, _| {
            if true || name == "common.glsl" && type_ == IncludeType::Relative {
                Ok(ResolvedInclude {
                    resolved_name: "std/foo.glsl".to_string(),
                    content: r#"
                    #ifndef FOO_H
                    #define FOO_H
                    #endif
                    "#
                    .to_string(),
                })
            } else {
                Err(format!("Couldn't find header \"{}\"", name))
            }
        });
    let binary_result = compiler.compile_into_spirv(
        source, ShaderKind::Vertex,
        "shader.glsl", "main", Some(&options)).unwrap();

    assert_eq!(Some(&0x07230203), binary_result.as_binary().first());

    let text_result = compiler.compile_into_spirv_assembly(
        source, ShaderKind::Vertex,
        "shader.glsl", "main", Some(&options)).unwrap();

    assert!(text_result.as_text().starts_with("; SPIR-V\n"));
     "fn answer() -> u32 { 42 }".parse().unwrap()
}
